import re
import string


def nettoyage_tweet(txt):
    # retirer les mentions d'user : @____
    txt = re.sub(r'@[A-Za-z0-9_]+', '', txt)
    # retirer les liens possibles
    txt = re.sub(r'https?://[A-Za-z0-9./]+', '', txt)
    # retirer les #
    txt = re.sub(r'#', '', txt)
    # retirer la ponctuation
    txt = txt.translate(str.maketrans('', '', string.punctuation))
    # retirer les chiffres
    txt = re.sub(r'\d+', '', txt)

    # passer le texte en minuscule
    txt = txt.lower()
    return txt

